#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "commons.h"

#define DEFAULT_SERVER_IP "127.0.0.1"
#define DEFAULT_SERVER_PORT 12345
#define KEY_LENGTH 2048

int connect_socket();
bool get_addr_from_user(struct sockaddr_in *addr);
bool verify_port(int port);
EVP_PKEY_CTX* init_crypto_ctx(EVP_PKEY *pkey);
EVP_PKEY* init_crypto();
EVP_PKEY* generateKeyPair(void);
bool send_public_key(int sock, EVP_PKEY *pkey);

void handleErrors(void) {
    ERR_print_errors_fp(stderr);
}

int main() {
    int sock;
    res_t res;
    uint8_t numbers_received[27], i = 0;
    uint16_t encrypted_data_len, decrypted_data_len;

    printf("CLIENTE\n");

    sock = connect_socket();
    if(!sock) return CRYPT_INIT_FAIL;

    EVP_PKEY *pkey = init_crypto();
    if(!send_public_key(sock, pkey)) return SEND_FAIL;

    EVP_PKEY_CTX *ctx = init_crypto_ctx(pkey);
    if(ctx == NULL){
        EVP_PKEY_free(pkey);
        return CRYPT_INIT_FAIL;
    } 

    unsigned char *encrypted_data = malloc(EVP_PKEY_size(pkey));
    unsigned char *decrypted_data = malloc(EVP_PKEY_size(pkey));

    while (i < 27) {
        encrypted_data_len = recv(sock, encrypted_data, EVP_PKEY_size(pkey), 0);
            
        if (encrypted_data_len < 0) {
            printf("Error al recibir datos");
            close(sock);
            res = RECEIVE_FAIL;
            goto clean_and_exit;
        } else if (encrypted_data_len == 0) {
            printf("Conexión cerrada por el servidor\n");
            res = SERVER_FAIL;
            goto clean_and_exit;
        } else {
            size_t decrypted_data_len_size = EVP_PKEY_size(pkey);
            if (EVP_PKEY_decrypt(ctx, decrypted_data, &decrypted_data_len_size, encrypted_data, encrypted_data_len) <= 0) {
                res = DECRYPT_FAIL;
                goto clean_and_exit;
            }

            numbers_received[i] = decrypted_data[0];  // Convertir char a int
            i++;
        }
    }

    printf("Números recibidos: ");
    for (int j = 0; j < 27; j++) {
        printf("%d ", numbers_received[j]);
    }
    printf("\nEsperando cierre del servidor...\n");
    getchar();

    char fin_buffer[1];
    if (recv(sock, fin_buffer, 1, 0) == 0) {
        printf("Segmento FIN recibido del servidor.\n");
        getchar();
        if (shutdown(sock, SHUT_WR) < 0) {
            printf("Error al enviar segmento FIN");
            res = CLOSING_FAIL;
        }
    }

clean_and_exit: 
    EVP_PKEY_free(pkey);
    EVP_cleanup();
    ERR_free_strings();
    EVP_PKEY_CTX_free(ctx);

    free(encrypted_data);
    free(decrypted_data);

    close(sock);

    return res;
}

EVP_PKEY* generateKeyPair(void) {
    bool res = false;
    EVP_PKEY_CTX *ctx = NULL;
    EVP_PKEY *pkey = NULL;

    ctx = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, NULL);
    if (ctx != NULL) {
        if (EVP_PKEY_keygen_init(ctx) > 0) {
            if (EVP_PKEY_CTX_set_rsa_keygen_bits(ctx, KEY_LENGTH) > 0) {
                if (EVP_PKEY_keygen(ctx, &pkey) > 0) {
                    res = true;
                }
            }
        }
    }

    EVP_PKEY_CTX_free(ctx);
    if (!res) {
        handleErrors();
        return NULL;
    }
    return pkey;
}

bool verify_port(int port) {
    return (port > 0 && port <= 65535);
}

bool get_addr_from_user(struct sockaddr_in *addr) {

    char server_ip[INET_ADDRSTRLEN];
    int server_port;

    printf("Ingrese la dirección del servidor, o presione \"enter\" para utilizar la configuracion default\n");

    if (fgets(server_ip, sizeof(server_ip), stdin) != NULL) {
        size_t len = strlen(server_ip);
        if (len > 0 && server_ip[len-1] == '\n') {
            server_ip[len-1] = '\0';
        }

        if (strlen(server_ip) == 0) {
            printf("Parametros default seleccionados: %s:%d\n\n", DEFAULT_SERVER_IP, DEFAULT_SERVER_PORT);
            strcpy(server_ip, DEFAULT_SERVER_IP);
            server_port = DEFAULT_SERVER_PORT;        
        }else{
            printf("Ingrese el puerto del servidor: ");
            scanf("%d", &server_port);
            if(verify_port(server_port) == false) {
                printf("Puerto inválido\n");
                return false;
            }
        }
    }else{
        printf("Error al leer la dirección del servidor\n");
        return false;
    }

    addr->sin_family = AF_INET;
    addr->sin_port = htons(server_port);
    if (inet_pton(AF_INET, server_ip, &(addr->sin_addr)) <= 0) {
        printf("Dirección inválida o no soportada");
        return false;
    }
    return true;
}

int connect_socket(){
    struct sockaddr_in server_addr;
    int sock;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Error al crear el socket");
        return 0;
    }

    if( get_addr_from_user(&server_addr) == false ){
        close(sock);
        return 0;
    }

    if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        printf("Error al conectarse al servidor");
        close(sock);
        return 0;
    }
    return sock;
}

bool send_public_key(int sock, EVP_PKEY *pkey) {

    BIO *pub = BIO_new(BIO_s_mem());
    PEM_write_bio_PUBKEY(pub, pkey);
    size_t pub_len = BIO_pending(pub);
    char *pub_key = malloc(pub_len + 1);
    BIO_read(pub, pub_key, pub_len);
    pub_key[pub_len] = '\0';

    bool res = true;

    if (send(sock, pub_key, pub_len, 0) < 0) {
        EVP_PKEY_free(pkey);
        printf("Error al enviar la clave pública");
        close(sock);
        res = false;
    }

    free(pub_key);
    BIO_free(pub);

    return res;
}

EVP_PKEY* init_crypto(){
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    return generateKeyPair();
}

EVP_PKEY_CTX* init_crypto_ctx(EVP_PKEY *pkey){
    EVP_PKEY_CTX *ctx = NULL;
    ctx = EVP_PKEY_CTX_new(pkey, NULL);
    if (!ctx || EVP_PKEY_decrypt_init(ctx) <= 0) return NULL;
    return ctx;
}