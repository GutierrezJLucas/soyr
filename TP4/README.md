## CONSIGNA 
### CLIENTE
Un cliente TCP que reciba números del 0 al 26 y los imprima en la salida
estándar.
1. Debe permitir al usuario especificar la ip y el puerto al que desea
conectarse. (Ayuda: tener cuidado con los formatos utilizados. Útil:
netinet/in.h, arpa/inet.h )
2. Debe desencriptar antes de imprimir. Para esto el cliente dispondrá de
las llaves pública y privada y antes de comenzar el intercambio deberá
enviarle la llave pública al servidor.
3. El cliente puede ejecutarse en un host a elección.
4. El cliente finaliza cuando recibe un FIN del servidor y deberá
responder con un FIN.

### SERVIDOR
 Un servicio TCP que envíe números de 0 a 26.
1. El servicio debe permitir conexiones desde un host remoto (ejemplo:
n9).
2. Los datos deben cifrarse antes de ser enviados para lo cual el servidor
utiliza la llave pública que recibió del cliente.
3. Luego de enviar los datos debe finalizar de manera ordenada enviando
un FIN al cliente.
4. El servicio puede ejecutarse en un host a elección diferente al cliente

# PROPUESTA 

## ARQUITECTURA

Se propone un sistema que pueda funcionar para múltiples clientes. Es decir, cada cliente se creará ejecutando el programa **client**.    
Luego el programa **server** debe manejar a todos estos clientes ejecutandose una vez. Para esto se utilizarán *procesos*.
Cada uno de estos procesos se creará cuando un cliente inicie una comunicación, y estarán encargados de recibir la información de cifrado y de procesar la secuencia de numeros con estos datos. 

## CIFRADO

Se utilizará un algoritmo de cifrado RSA, mediante la libreria OpenSSL. Esto permitirá al cliente generar la clave privada y pública de forma dinámica. Es decir, cada vez que se ejecute el cliente se creara un nuevo par. De esta forma solo el proceso del servidor que tome esta "sesion" puede responderle apropiadamente.

## ENTRADA DE DATOS

Los datos de IP y puerto provistos por el usuario del cliente seran verificados.
De no proveerse ningun dato, simplemente presionando enter, se utilizarán los datos por defecto. 

# USO

## COMPILACION
 
Para compilar el proye se puede hacer uso del Makefile, tipeando: 

Preparacion: 

```
make clean
```

Compilar el cliente: 

```
make client
```

Compilar el servidor:

```
make server
```

Compilar todo:

```
make all
```

## EJECUCION

En primer lugar se debe ejecutar el servidor. 
Para esto, ubicarse en el directorio raiz del proyecto y tipear: 

```
./server
```

Este quedará esperando a que un cliente se conecte. 

En sgundo lugar ejecutar una instancia del cliente: 

Luego de compilar ejecutar:

```
./client
```

Luego escibir la direccion IP del servidor en cuestion con el formato:

> xxx.xxx.xxx.xxx

Finalmente escribir el puerto deseado. 

Si los datos provistos son invalidos en su formato, el cliente informara el error y se terminara a si mismo. 

Si se desea utilizar los datos por defecto, ejecutar el cliente y cuando se requiera ingresar el IP simplemente presionar *enter*.


Pueden ejecutarse multiples instancias del cliente, accediendo nuevamente a su programa. 

Una una ejecutada esta secuencia, el servidor iniciara una secion y se producira el intercambio de claves y la informacion requerida. 
Para dar cuenta de que efectivamente se genera un thread nuevo por cada cliente, se requiere que del lado del servidor se presione *enter* para dar la señal de fin desde el servidor al cliente. 
