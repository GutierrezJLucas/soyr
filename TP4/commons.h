#ifndef __RES_TYPES_H__
#define __RES_TYPES_H__

typedef enum {
    CLOSING_FAIL,
    CRYPT_INIT_FAIL,
    SEND_FAIL, 
    RECEIVE_FAIL,
    DECRYPT_FAIL, 
    ENCRYPT_FAIL,
    SERVER_FAIL,
    END_ACK_FAIL,
    OK
} res_t;

#define DEFAULT_SERVER_PORT 12345

#endif