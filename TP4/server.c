#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/evp.h>

#include "commons.h"

#define BACKLOG 5
#define KEY_LENGTH 2048

void handleErrors(void) {
    ERR_print_errors_fp(stderr);
}

EVP_PKEY* loadPublicKey(const char* pub_key_str);
EVP_PKEY* get_client_public_key(int sock);
int connect_and_listen();
void* handle_client(void* arg);

int main() {
    int server_sock, *client_sock;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len;
    pthread_t thread_id;

    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();

    server_sock = connect_and_listen();
    if(server_sock == 0) return SERVER_FAIL;

    while (1) {
        client_addr_len = sizeof(client_addr);
        client_sock = malloc(sizeof(int));
        if ((*client_sock = accept(server_sock, (struct sockaddr *)&client_addr, &client_addr_len)) < 0) {
            printf("Error al aceptar la conexión");
            free(client_sock);
            continue;
        }

        if (pthread_create(&thread_id, NULL, handle_client, (void*)client_sock) < 0) {
            printf("Error al crear el proceso");
            free(client_sock);
            continue;
        }else{
            printf("\nNuevo cliente conectado, asignado al thread: %d\n", (unsigned long int)thread_id);
        }

        pthread_detach(thread_id);  // Para evitar memory leaks
    }

    close(server_sock);

    EVP_cleanup();
    ERR_free_strings();

    return OK;
}

void* handle_client(void* arg) {
    int client_sock = *((int*)arg);
    free(arg);
    res_t res;
    EVP_PKEY* client_pub_key = NULL;
    EVP_PKEY_CTX* ctx = NULL;
    unsigned char* encrypted_data = NULL;
    size_t encrypted_data_len;
    int numbers_to_send[27];
    int i;

    client_pub_key = get_client_public_key(client_sock);

    encrypted_data = malloc(EVP_PKEY_size(client_pub_key));
    ctx = EVP_PKEY_CTX_new(client_pub_key, NULL);

    if (ctx == NULL || EVP_PKEY_encrypt_init(ctx) <= 0) {
        printf("Error al inicializar el contexto de cifrado");
        res = CRYPT_INIT_FAIL;
        goto clean_and_exit;
    }

    for (i = 0; i < 27; i++) {
        numbers_to_send[i] = i;
        if (EVP_PKEY_encrypt(ctx, encrypted_data, &encrypted_data_len, (unsigned char*)&numbers_to_send[i], sizeof(numbers_to_send[i])) <= 0) {
            res = ENCRYPT_FAIL;
            goto clean_and_exit;
        }

        if (send(client_sock, encrypted_data, encrypted_data_len, 0) < 0) {
            printf("Error al enviar datos cifrados");
            res = SEND_FAIL;
            goto clean_and_exit;
        }
    }

    printf("Press key to continue");
    getchar();

    if (shutdown(client_sock, SHUT_WR) < 0) {
        printf("Error al enviar segmento FIN");
        res = END_ACK_FAIL;
    }else{
        res = OK;
    }

clean_and_exit: 
    handleErrors();
    close(client_sock);
    EVP_PKEY_free(client_pub_key);
    EVP_PKEY_CTX_free(ctx);
    free(encrypted_data);

    pthread_exit(&res);
}

EVP_PKEY* loadPublicKey(const char* pub_key_str) {
    BIO* bio = BIO_new_mem_buf((void*)pub_key_str, -1);
    EVP_PKEY* pub_key = PEM_read_bio_PUBKEY(bio, NULL, NULL, NULL);
    BIO_free(bio);
    if (!pub_key) {
        handleErrors();
    }
    return pub_key;
}

EVP_PKEY* get_client_public_key(int sock){
    char pub_key_buffer[2048];
    int pub_key_len = recv(sock, pub_key_buffer, sizeof(pub_key_buffer) - 1, 0);
    if (pub_key_len < 0) {
        perror("Error al recibir la clave pública");
        close(sock);
        return NULL;
    }
    pub_key_buffer[pub_key_len] = '\0';
    return loadPublicKey(pub_key_buffer);
}

int connect_and_listen(){
    int server_sock = 0;
    struct sockaddr_in server_addr, client_addr;

    if ((server_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Error al crear el socket");
        return SERVER_FAIL;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(DEFAULT_SERVER_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    // Configurar la opción SO_REUSEADDR
    int opt = 1;
    if (setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) {
        perror("setsockopt");
        close(server_sock);
        return SERVER_FAIL;
    }

    if (bind(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Error al enlazar el socket");
        close(server_sock);
        return SERVER_FAIL;
    }

    if (listen(server_sock, BACKLOG) < 0) {
        perror("Error al escuchar en el socket");
        close(server_sock);
        return SERVER_FAIL;
    }

    printf("Servidor escuchando en el puerto %d...\n", DEFAULT_SERVER_PORT);

    return server_sock;
}
