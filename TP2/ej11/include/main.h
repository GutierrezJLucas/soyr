#ifndef ___MAIN__
#define ___MAIN__

#define WRITE_DELAY_S 1
//#define DEBUG_EN

typedef enum{
	ERR_KEY         = 1,
	ERR_IDENTIFIER  = 2,
	ERR_MEM_POINTER = 3,
	ERR_ENCRYPT_KEY = 4, 
	OK              = 0
}err_t;

typedef enum{
	CONTINUE_SIGNAL,
	TERMINATE_SIGNAL,
	SIGNAL_ERR
}signal_t;

typedef struct{
	uint8_t key_1;
	uint8_t key_2;
}encryp_key_t;

err_t sub_process();
err_t main_process(pid_t sp_pid);
encryp_key_t* get_public_keys();
err_t load_public_keys();

const char* FILE_NAME_1 = "/bin/more";
const char* FILE_NAME_2 = "/bin/less";
const int NUM_1 = 123;
const int NUM_2 = 456;
const uint8_t MESSAGE_SIZE_1 = 20;
const uint8_t MESSAGE_SIZE_2 = 2;

#endif
