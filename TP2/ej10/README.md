Compilar manualmente y ejecutar con el siguiente comando:

```
gcc -Wall -std=c11 -D_POSIX_SOURCE tp2.c -o tp2 && ./tp2
```

O compilar y ejecutar haciendo uso del Makefile simplemente con el comando:

```
make && ./tp2
```

Para probar el normal funcionamiento del programa se puede hacer uso del script llamado 'run.sh' localizado en el mismo directorio que el codigo fuente.

Para ello utilizar el comando:

```
./run.sh
```

Si previamente no se compilo y ejectuo el codigo fuente, se observara una leyenda que dice:

>No process named tp2

Esto significa que no hay ningun proceso dentro de la lista de procesos activos que se llame tp2, y por tanto no puede hacerse uso de su PID. 

De existir una instancia del progama en ejecucion se observara el siguiente flujo de ejecucion:

1) Se envia una señal SIGUSR1 al PID principal creando un subproceso
2) Se envia una señal SIGUSR2 al PID principal creando un subproceso que imprime la fecha y luego se termina
3) Se espera 5 segundos
4) Se repite de 1 a 3, 3 veces mas, creando 4 subprocesos e imprimiendo la fecha de creacion luego de cada uno
5) Se espera 3 segundos
6) Se terminan todos los procesos enviando SIGTERM al PID principal

Luego se verificara que no exista ningun proceso llamado "tp2". 
Si esta verificacion falla se leer la leyenda:

> Some tp2 process left. Check manually

Caso contrario:

> All tp2 process terminated. All good
