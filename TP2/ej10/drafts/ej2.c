#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void){
	char program[]="/bin/ls";
	char *args[] = {"/bin/ls","-l","--color",NULL};

	pid_t son_id;

	son_id = fork();
	
	if(son_id == 0){
		int input=0;
		execv(program,args);
		printf("Pres any key to return...");
		scanf("%d", &input);
		return input;
	}else{
		int son_exit_code=0;
		waitpid(son_id, &son_exit_code, 0);
		printf("Son returned with code: %d\n", son_exit_code);
	}			
		
	return(1);
}

