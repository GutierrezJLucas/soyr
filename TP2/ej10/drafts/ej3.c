#define _POSIX_C_SOURCE 200112L
#define _OPEN_THREADS

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

pid_t son_id;

void sigalarm_handler(){
	printf("Iam a redefined handler conceived to signaling my suprocess\n");
	kill(son_id, SIGUSR1);
}

int main(void){

	sigset_t signal_set;
	int sig_ret;

	sigemptyset(&signal_set);
	if(sigaddset(&signal_set, SIGUSR1) == -1){
		printf("Error adding SIGUSR1 signal to set. Terminating program\n");
		return 0;
	}
	son_id = fork();
	if(son_id == 0){
		printf("I am a subprocces (%d) and will only be setting a 2 second alarm.\n", getpid());
		sigset_t subp_signal_set;
		sigemptyset(&subp_signal_set);
		sigaddset(&subp_signal_set, SIGALRM);

		if(sigsuspend(&subp_signal_set) != -1) printf("Error suspending SIGALRM signal\n");

		alarm(2);
		
		if(sigwait(&signal_set, &sig_ret) != SIGUSR1){
			printf("Error in sigwait for SIGUSR1\n");
			return 20;
		}else{
			printf("Sigwait terminated by %d\n", sig_ret);
			return 19;
		}
	}else{
		int son_exit_code=0;
		printf("I am the main (%d) process and will only wait for my subprocess alarm  \n", getpid());
		signal(SIGALRM, sigalarm_handler);
		alarm(20);
		waitpid(son_id, &son_exit_code, 0);
		if(WIFEXITED(son_exit_code)){
			printf("Subprocess returned successfully. Return status is %d\n", WEXITSTATUS(son_exit_code));
		}else{
			if(WIFSIGNALED(son_exit_code)){	
				printf("Subprocess terminated by signal.\n");
				int terminating_signal = WTERMSIG(son_exit_code);
					if(terminating_signal == SIGALRM){
						printf("Termianted by SIGALRM. Redefining handler and waiting for alarm\n");
					}else{	
						printf("Signal ID: %d\n", WTERMSIG(son_exit_code));				
					}	
			}
		}
	}			
		
	return(1);
}

