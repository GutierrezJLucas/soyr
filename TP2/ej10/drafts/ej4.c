#define _POSIX_C_SOURCE 200112L
#define _OPEN_THREADS

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdbool.h>

struct subprocess_log{
	int counter;
	int pid_register[10];
}current_log;


void alarm_set_and_fire();
void sigusr1_handler();
void sigusr2_handler();
void sigalarm_handler();

void sigusr1_handler(){
	printf("SIGUSR1 handler\n");
}

void sigusr2_handler(){
	printf("SIGUSR2 handler of death\n");
	for(int i=0; i<current_log.counter; i++){
			kill(SIGKILL,current_log.pid_register[i]);
	}	
}

void sigalarm_handler(){

	alarm_set_and_fire();	
	
	if(current_log.counter == 0){
		printf("No subprocess created\n");
		return;
	}
	for(int i=0; i<current_log.counter; i++){
		printf("Subprocess #%d's ID: %d\n", i, current_log.pid_register[i]);
	}
}

void alarm_set_and_fire(){
	signal(SIGALRM, &sigalarm_handler);
	alarm(5);
}

int main(void){

	sigset_t signal_set;
	pid_t son_id;
	int sig_ret;
	son_id = -1;
	current_log.counter = 0;

	signal(SIGUSR1, &sigusr1_handler);
	signal(SIGUSR2, &sigusr2_handler);
	
	printf("I am the main (%d) process and will only wait for my subprocess alarm  \n", getpid());
	
	sigemptyset(&signal_set);
	if(sigaddset(&signal_set, SIGUSR1) == -1){
		printf("Error adding SIGUSR1 signal to set. Terminating program\n");
		return 0;
	}
	alarm_set_and_fire();	
	while(true){
		if(sigwait(&signal_set, &sig_ret) == -1){
			printf("Error in sigwait for SIGUSR1\n");
		}else{
			printf("Sigwait terminated by SIGUSR1 (%d)\n", sig_ret);
			son_id = fork();
			if(son_id == 0){
				printf("Iam a subprocess #%d, created from %d\n", getpid(), getppid());
			}else{
				current_log.pid_register[current_log.counter] = son_id;
				current_log.counter++;
			}
		}
	}
/*
	son_id = fork();
	if(son_id == 0){
		printf("I am a subprocces (%d) and will only be setting a 2 second alarm.\n", getpid());
		sigset_t subp_signal_set;
		sigemptyset(&subp_signal_set);
		sigaddset(&subp_signal_set, SIGALRM);

		if(sigsuspend(&subp_signal_set) != -1) printf("Error suspending SIGALRM signal\n");

		alarm(2);
		
		if(sigwait(&signal_set, &sig_ret) != SIGUSR1){
			printf("Error in sigwait for SIGUSR1\n");
			return 20;
		}else{
			printf("Sigwait terminated by %d\n", sig_ret);
			return 19;
		}
	}else{
		int son_exit_code=0;
		printf("I am the main (%d) process and will only wait for my subprocess alarm  \n", getpid());
		signal(SIGALRM, sigalarm_handler);
		alarm(20);
		waitpid(son_id, &son_exit_code, 0);
		if(WIFEXITED(son_exit_code)){
			printf("Subprocess returned successfully. Return status is %d\n", WEXITSTATUS(son_exit_code));
		}else{
			if(WIFSIGNALED(son_exit_code)){	
				printf("Subprocess terminated by signal.\n");
				int terminating_signal = WTERMSIG(son_exit_code);
					if(terminating_signal == SIGALRM){
						printf("Termianted by SIGALRM. Redefining handler and waiting for alarm\n");
					}else{	
						printf("Signal ID: %d\n", WTERMSIG(son_exit_code));				
					}	
			}
		}
	}			
*/		
	return(1);
}

