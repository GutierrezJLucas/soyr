#define _POSIX_C_SOURCE 200112L   // necessary for sigwait usage

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h> 
#include <signal.h>
#include <stdbool.h>

#define MAX_SUBPROCESS 20
#define ALARM_TIMEOUT   5

struct subprocess_log{
	int counter;
	int pid_register[MAX_SUBPROCESS];
}current_log;

char CMN_GETTIME[]="/bin/date";
char CMN_TIMEZONE[]="TZ=\"America/Argentina/Buenos_Aires\"";
char *ARG_GETTIME[] = {"/bin/date", NULL};
char *ARG_TIMEZONE[] = {"TZ=\"America/Argentina/Buenos_Aires\"", NULL};

void alarm_set_and_fire(); // restars the alarm handler and sets the timeout to ALARM_TIMEOUT
void sigalarm_handler();   // calls alarm_set_and_fire
void empty_handler(){}     // empty handler just for the sake of suspend the 'terminate' default action of signals
void terminate_sb();       // terminates every subprocess registered in the log struct
bool add_sp_to_log(pid_t new_sp);   // checks if there is room for a new subprocess and register it in the log struct
bool config_signals();     // config signal handlers and loads the signals to use in  signal_set 

sigset_t signal_set;

int main(void){

	pid_t sp_id;
	current_log.counter = 0;

	if(config_signals() == false){
		printf("Some error occured while configuring the signals. Terminating program.\n");
		goto exit_fail;     // it could also return from here, but I like to have only one point of return for the same cases.
	}
	
	printf("Main process #%d. Will wait for a subprocess creation alarm\n", getpid());
	int sig_ret;
	bool timezone_configured = false;
	while(true){
		if(sigwait(&signal_set, &sig_ret) == -1){
			printf("Error in sigwait. Terminating program.\n");
			goto exit_fail;
		}else{
			switch(sig_ret){
			
				case(SIGUSR1):
					//printf("Sigwait terminated by SIGUSR1\n");
					sp_id = fork();
					if(sp_id == 0){
						alarm_set_and_fire();	// first use of SIGALRM, configure handler and set timeout
						while(true){
							pause();        // eternally waiting for SIGALRM
						}
					}else{
						add_sp_to_log(sp_id);   // if this is the main process, then log the just created subprocess
					}
					break;

				case(SIGUSR2):
					//printf("Sigwait terminated by SIGUSR2\n");
					sp_id = fork();
					if(sp_id == 0){
						printf("-----------\n");
						printf("Subprocess #%d, prompting current time and date for GTM-3\n", getpid());
						if(timezone_configured == false){
							timezone_configured = true;
							execv(CMN_TIMEZONE,ARG_TIMEZONE);
						}
						execv(CMN_GETTIME,ARG_GETTIME);
						return 0;      // print data and kill process returning
					}
					break;
				
				case(SIGTERM):
					printf("SIGTERM signal received. Terminating everything on register\n");
					terminate_sb();
					goto exit_ok;      // it may seem not-elegant to use goto, but its a good practice to exit a nested while()
					break;

				default: break;
			}
		}
	}
exit_ok:	
	return(1);
exit_fail:
	return(0);
}

void terminate_sb(){
	int aux_counter = current_log.counter;
	for(int i=0; i<=aux_counter; i++){
		printf("Terminating subprocess #%d's ID: %d\n", i, current_log.pid_register[i]);
		kill(current_log.pid_register[i], SIGKILL);
		current_log.pid_register[i] = 0; // not very convenient as 0 is a valid id
		current_log.counter--;
	}	
}

void sigalarm_handler(){
	printf("Subprocess #%d, created from %d\n", getpid(), getppid());
	alarm_set_and_fire();
}

void alarm_set_and_fire(){
	signal(SIGALRM, &sigalarm_handler);
	alarm(ALARM_TIMEOUT);
}

bool add_sp_to_log(pid_t new_sp){
	if (current_log.counter == MAX_SUBPROCESS){
		printf("Max suprocess alowed reached\n");
		return false;
	}	
	current_log.pid_register[current_log.counter] = new_sp;
	current_log.counter++;
	return true;
}

bool config_signals(){
	if(signal(SIGUSR1, &empty_handler) == SIG_ERR)	return false;
	if(signal(SIGUSR2, &empty_handler) == SIG_ERR)	return false;
	if(signal(SIGTERM, &empty_handler) == SIG_ERR)	return false;
	if(sigemptyset(&signal_set) == -1)	        return false;
	if(sigaddset(&signal_set, SIGUSR1) == -1)	return false;
	if(sigaddset(&signal_set, SIGUSR2) == -1)	return false;
	if(sigaddset(&signal_set, SIGTERM) == -1)	return false;

	return true;
}

