parent_pid="$(ps -a | grep -e "tp2" | awk '{print $1}' | head -1)"
if [ -z "$parent_pid" ]; then
	echo "No process named tp2"
	exit 0
else
	echo "Process named tp2 found, its PID is "$parent_pid""
	echo "Creating sub-process by sending SIGUSR1 signals"
	echo "Then timestamping the creation by creating a date-printing process with SIGUSR2 signal"
	for i in {1..4}
	do
		echo "Creating $i/4 and waiting 5 seconds"
		kill -s SIGUSR1 $parent_pid 
		sleep .1
		kill -s SIGUSR2 $parent_pid 
		sleep 4.9 
	done
	
	echo "Waiting 3 seconds before terminating all subprocess by sending SIGTERM signal"
	sleep 3
	kill -s SIGTERM $parent_pid 
fi

sleep 1

parent_pid="$(ps -a | grep -e "tp2" | awk '{print $1}' | head -1)"
if [ -z "$parent_pid" ]; then
	echo "All tp2 process terminated. All good"
	exit 1
else
	echo "Some tp2 process left. Check manually"
	exit 0
fi
