#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

// #define BIG_ENDIAN

#define FRAME_SIZE 16 

typedef union data_u{
    struct{
        uint8_t a;
        uint8_t b;
        uint8_t c;
        uint8_t d;
    }byte_part;
    uint32_t byte_32;
}data_u;
typedef struct data_str{
    uint32_t id;
    uint16_t pressure;
    int16_t temperature;
    uint16_t rainfall;
    uint8_t humidity;
    struct timestamp{
        uint32_t epoch;
        char str[21];
    }timestamp;
}data_str;

uint32_t get_id(char* buffer){
#ifdef BIG_ENDIAN
    return (uint32_t)((uint32_t)((uint8_t)buffer[3] << 24) | (uint32_t)((uint8_t)buffer[2] << 16) | (uint16_t)((uint8_t)buffer[1] << 8) | (uint8_t)buffer[0]);
#else
    return (uint32_t)((uint32_t)((uint8_t)buffer[3] << 24) | (uint32_t)((uint8_t)buffer[2] << 16) | (uint16_t)((uint8_t)buffer[1] << 8) | (uint8_t)buffer[0]);
#endif
}

uint16_t get_pressure(char* buffer){
    // printf("buffer[4]: %d\r\n", (uint8_t)buffer[5]);
    // printf("buffer[4] << 8: %u\r\n", (uint16_t)((uint8_t)buffer[5]) << 8);
    // printf("buffer[5]: %d\r\n", (uint8_t)buffer[4]);
    // printf("buffer[4] << 8 | buffer[5]: %d\r\n", (uint16_t)((uint8_t)buffer[5] << 8) | (uint8_t)buffer[4]);
#ifdef BIG_ENDIAN
    return (uint16_t)((uint8_t)buffer[4] << 8) | (uint8_t)buffer[5];
#else
    return (uint16_t)((uint8_t)buffer[5] << 8) | (uint8_t)buffer[4];
#endif
}

int16_t get_temperature(char* buffer){
#ifdef BIG_ENDIAN
    return (int16_t)((int8_t)buffer[6] << 8) | (uint8_t)buffer[7];
#else
    return (int16_t)((int8_t)buffer[7] << 8) | (uint8_t)buffer[6];
#endif
}

uint16_t get_rainfall(char* buffer){
#ifdef BIG_ENDIAN
    return (uint16_t)((uint8_t)buffer[8] << 8) | (uint8_t)buffer[9];
#else
    return (uint16_t)((uint8_t)buffer[9] << 8) | (uint8_t)buffer[8];
#endif
}

uint8_t get_humidity(char* buffer){
    return (uint8_t)(buffer[10]);
}

uint32_t get_timestamp_epoch(char* buffer){
    // data_u timestamp;
    // timestamp.byte_part.a = (uint8_t)buffer[11];
    // timestamp.byte_part.b = (uint8_t)buffer[12];
    // timestamp.byte_part.c = (uint8_t)buffer[13];
    // timestamp.byte_part.d = (uint8_t)buffer[14];

    // printf("timestamp.byte_32: %u\r\n", timestamp.byte_32);
    // return timestamp.byte_32;
#ifdef BIG_ENDIAN
    return (uint32_t)((uint32_t)((uint8_t)buffer[11] << 24) | (uint32_t)((uint8_t)buffer[12] << 16) | (uint16_t)((uint8_t)buffer[13] << 8) | (uint8_t)buffer[14]);
#else
    return (uint32_t)((uint32_t)((uint8_t)buffer[14] << 24) | (uint32_t)((uint8_t)buffer[13] << 16) | (uint16_t)((uint8_t)buffer[12] << 8) | (uint8_t)buffer[11]);
#endif
}

void convert_timestamp(uint32_t timestamp, char* str){
    printf("timestamp: %u\r\n", timestamp);
    struct tm time = {0};
    time.tm_year = 2000 - 1970;  
    time.tm_mon = 0;           
    time.tm_mday = 1;           
    time.tm_hour = 0;
    time.tm_min = 0;
    time.tm_sec = 0;

    time_t epoch = mktime(&time);
    time_t full_time = epoch + timestamp;
    struct tm *readable_time = localtime(&full_time);
    strftime(str, 21, "%Y-%m-%d,%H:%M:%S", readable_time);
}

void get_data(char* buffer, data_str* data){
    data->id = get_id(buffer);
    data->pressure = get_pressure(buffer);
    data->temperature = get_temperature(buffer);
    data->rainfall = get_rainfall(buffer);
    data->humidity = get_humidity(buffer);
    data->timestamp.epoch = get_timestamp_epoch(buffer);
    convert_timestamp(data->timestamp.epoch, data->timestamp.str);
}

void store_data_in_csv(const data_str *data, const char *filename) {
    FILE *csv_file = fopen(filename, "a");
    if (csv_file == NULL) {
        perror("Error opening CSV file");
        return;
    }

    fprintf(csv_file, "%ld,%s,%2.1f,%3.1f,%2.1f,%03d\n",
            data->id,
            data->timestamp.str,
            (float)(data->temperature/10),
            (float)(data->pressure/10),
            (float)(data->rainfall/10),
            data->humidity
            );

    if (fclose(csv_file) != 0) {
        perror("Error closing CSV file");
    }
}

void print_data(const data_str *data) {
    printf("ID: %d\n", data->id);
    printf("Pressure: %3.1f\n", (float)(data->pressure/10));
    printf("Temperature: %2.1f\n", (float)(data->temperature/10));
    printf("Rainfall: %2.1f\n", (float)(data->rainfall/10));
    printf("Humidity: %d\n", data->humidity);
    printf("Timestamp: %s\n", data->timestamp.str);
    printf("--\n");
}

int main() {
    FILE *file;
    char buffer[FRAME_SIZE];
    size_t bytesRead;

    data_str data;

    file = fopen("./datos.bin", "rb");
    if (file == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }
    while ((bytesRead = fread(buffer, 1, FRAME_SIZE, file)) > 0) {
        for(uint8_t i = 0; i < bytesRead; i++){
            printf("[%d] %c %d\r\n", i, buffer[i], (uint8_t)buffer[i]);
        }
        getchar();
        get_data(buffer, &data);
        store_data_in_csv(&data, "data.csv");
        print_data(&data);
    }

    if (fclose(file) != 0) {
        perror("Error closing file");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
