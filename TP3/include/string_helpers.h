#ifndef ___STRING_HELPERS__
#define ___STRING_HELPERS__

//#define DEBUG_EN

uint8_t* encode_string(char* input_string, uint8_t size);
char* decode_string(uint8_t* input_code, uint8_t size);
char* normalize(char* target, uint8_t size);
bool is_a_letter(char letter);
bool is_capitalized(char letter);
uint8_t iterate_encoded_letter(uint8_t encoded_letter);

bool is_capitalized(char letter){
	return( ((char)letter < 91) ? true : false );
}

bool is_a_letter(char letter){
	uint8_t int_letter = (char)letter;

	if ( (int_letter > 122 || int_letter  < 65) || (int_letter  > 90 && int_letter < 97) ){
		return false;
	}else{
		return true;
	}
}

char* normalize(char* target, uint8_t size){
	char* output_string = (char*)malloc(size*sizeof(char));
	memset(output_string, 0, sizeof(char)*size);
	
	uint8_t int_char;
	
	for (uint8_t i=0; i<size; i++) {
		int_char = *(target+i); 
		if (is_a_letter(int_char) == false) {
			*(output_string+i) = ' ';
		}else if (is_capitalized(int_char) == false) {
			*(output_string+i) = (char)int_char;
		}else{
			*(output_string+i) = (char)(int_char + 32 );	
		}
	}
	*(output_string+size) = '\0';

	return(output_string);
}

char decode_letter(uint8_t coded_letter){
	return(coded_letter == 0 ? ' ' : (char)(coded_letter+96));
}

char* decode_string(uint8_t* input_code, uint8_t size){
	char* decoded_string = (char*)malloc((size + 1) * sizeof(char));
		
	for (uint8_t i=0; i<size; i++){
		*(decoded_string+i) = decode_letter(*(input_code+i)); 
	}
	*(decoded_string+size) = '\0';

	return decoded_string;
}

uint8_t iterate_encoded_letter(uint8_t encoded_letter){
	uint8_t aux = encoded_letter;
	if(encoded_letter == 26){
		return 1;
	}else{
		aux++;
		return aux;
	}
}

uint8_t encode_letter(char letter){
	return(letter == ' ' ? 0 : (uint8_t)(letter)-96);
}

uint8_t* encode_string(char* input_string, uint8_t size){

	uint8_t* encoded_string = (uint8_t*)malloc(size*sizeof(uint8_t));
	
	char* normal_string = normalize(input_string, size);
#ifdef DEBUG_EN
	printf("input string = %s\n", input_string);
	printf("output string = %s\n", normal_string);
	printf("coded int-array = ");
#endif

	for (uint8_t i=0; i<size; i++){
		*(encoded_string+i) = encode_letter(*(normal_string+i));
#ifdef DEBUG_EN
		printf("0x%02X ", *(encoded_string+i));
#endif
	}

#ifdef DEBUG_EN
	printf("\n");
#endif
	free(normal_string);
	return encoded_string;
}
#endif
