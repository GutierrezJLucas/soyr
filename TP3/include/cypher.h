#ifndef ____CYPHER__
#define ____CYPHER__

uint8_t cypher_fnc(uint8_t value, uint8_t public_key_1, uint8_t public_key_2);
uint8_t decypher_fnc(uint8_t value, uint8_t private_key_1,uint8_t private_key_2);
uint8_t* cypher(uint8_t* in_message, uint8_t size, uint8_t public_key_1, uint8_t public_key_2);

uint8_t cypher_fnc(uint8_t value, uint8_t public_key_1, uint8_t public_key_2){
	return((public_key_1*value+public_key_2)%(27));
}

uint8_t decypher_fnc(uint8_t value, uint8_t private_key_1,uint8_t private_key_2){
	return((private_key_1*value+private_key_2)%(27));
}

uint8_t* cypher(uint8_t* in_message, uint8_t size, uint8_t public_key_1, uint8_t public_key_2){
	uint8_t* out_buffer;
	out_buffer = (uint8_t*)malloc(size*sizeof(uint8_t));
	if (out_buffer == NULL) return NULL;
	
	for(uint8_t i=0;i<size;i++){
		*(out_buffer+i) = cypher_fnc(*(in_message+i), public_key_1, public_key_2);
	}
	return out_buffer;
}

#endif
