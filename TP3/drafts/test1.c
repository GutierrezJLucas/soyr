#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/wait.h> 
#include <signal.h>
#include <stdbool.h>

const char* FILE_NAME = "/bin/more";
const int NUM = 123;

int main_process(){
	printf("I am the main process\n");

	key_t memory_key;	
	int  memory_id;
	long int* memory_ptr;

	memory_key = ftok(FILE_NAME, NUM);
	if(memory_key == -1){
		printf("Error creating a memory key. Finishing program.\n");
		return 0;
	}

	memory_id = shmget(memory_key, 50*sizeof(long), 0666|IPC_CREAT);
	if(memory_id == -1){
		printf("Error getting a memry section. Finishing program.\n");
		return 1;
	}

	memory_ptr = (long *)shmat(memory_id, (const void *)0, 0);
	if(memory_ptr == NULL){
		printf("Error getting a memry section. Finishing program.\n");
		return 2;
	}
	
	for(short i=0; i<=50; i++){
		*(memory_ptr+i) = i;
		sleep(1);
	}
	
	shmdt((const void*) memory_ptr);	
	return -1;
}

int main(int argc, char **argv){
	main_process();

}
