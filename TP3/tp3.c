#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <string.h>
#include <sys/wait.h> 
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>

#include "include/string_helpers.h"
#include "include/cypher.h"

#define WRITE_DELAY_S 1
#define DEBUG_EN

typedef enum{
	ERR_KEY         = 1,
	ERR_IDENTIFIER  = 2,
	ERR_MEM_POINTER = 3,
	ERR_ENCRYPT_KEY = 4, 
	ERR_PARAM	= 5,
	ERR_PID		= 6,
	OK              = 0
}err_t;

typedef enum{
	CONTINUE_SIGNAL,
	TERMINATE_SIGNAL,
	SIGNAL_ERR
}signal_t;

typedef struct{
	uint8_t key_1;
	uint8_t key_2;
}encryp_key_t;

err_t sub_process();
err_t main_process();
pid_t get_partner_pid(char partner);
err_t set_mysm_pid(char my_id, pid_t my_pid);
encryp_key_t* get_public_keys();
err_t load_public_keys();

const char* FILE_NAME_1 = "/bin/more";
const char* FILE_NAME_2 = "/bin/less";
const char* FILE_NAME_3 = "/bin/more";
const int NUM_1 = 123;
const int NUM_2 = 456;
const int NUM_3 = 789;
const uint8_t MESSAGE_SIZE_1 = 20;
const uint8_t MESSAGE_SIZE_2 = 2;
const uint8_t MESSAGE_SIZE_3 = 4;

bool continue_flag = false, terminate_flag = false;

void sigusr1_handler(){
#ifdef DEBUG_EN
	printf("SIGUSR1 signal received in %d process. Setting CONTINUE flag\n", getpid());
#endif
	continue_flag = true;
}

void sigterm_handler(){
#ifdef DEBUG_EN
	printf("SIGTERM signal received in %d process. Setting TERMINATE flag\n", getpid());
#endif
	terminate_flag = true;
}

signal_t wait_for_signal(){
	
	signal(SIGUSR1, &sigusr1_handler);
	signal(SIGTERM, &sigterm_handler);
#ifdef DEBUG_EN
	printf("%d process waiting for signal to continue\n", getpid());
#endif
	pause();
	
	if (terminate_flag == true){
		terminate_flag = false;
#ifdef DEBUG_EN
		printf("Terminate signal received\n");
#endif
		return TERMINATE_SIGNAL;
	}else if (continue_flag == true){
		continue_flag = false;
#ifdef DEBUG_EN
		printf("Continue signal received\n");
#endif
		return CONTINUE_SIGNAL;
	}else{
		printf("Unknown signal received\n");
		return SIGNAL_ERR;
	}
}

err_t main_process(){

	pid_t my_pid = getpid();	
	printf("Process A pid:  %d\n", my_pid);

	printf("Saving current pid into shared memory\n");
	if (set_mysm_pid('A', my_pid) != OK) return ERR_PID;
	
	printf("Waiting for Process B\n");
	wait_for_signal();

	printf("Searching for Process B pid in shared memory\n");

	pid_t partner_pid;
	while(true){
		partner_pid = get_partner_pid('B');
		if (partner_pid == 0){
			printf("Failed.. retrying in 1 second\n");
			sleep(1);
		}else{
			printf("Success. Process B pid: %d\n", partner_pid);
			break;
		}
	}

	// get public key from shared memory
	encryp_key_t* public = get_public_keys();            // get public encryption keys from shared memory
	if (public == NULL) return(ERR_ENCRYPT_KEY);
	encryp_key_t private = { .key_1 = 7, .key_2 = 19};   // load private encryption key from local function
	
	kill(partner_pid, SIGUSR1);

	// get shared memory
	printf("Getting shared memory\n");
	key_t memory_key;	
	int  memory_id;
	uint8_t* memory_ptr;

	memory_key = ftok(FILE_NAME_1, NUM_1);
	if (memory_key == -1) return(ERR_KEY);
	memory_id = shmget(memory_key, (MESSAGE_SIZE_1)*sizeof(uint8_t), 0666|IPC_CREAT);
	if (memory_id == -1) return(ERR_IDENTIFIER);
	memory_ptr = (uint8_t*)shmat(memory_id, (const void *)0, 0);
	if (memory_ptr == NULL) return(ERR_MEM_POINTER);

	// normalize string taking away non letter characters
	uint8_t* encrypted_string = encode_string("fIrStval A", sizeof("fIrStval A"));

	while(true){
		// encrypt message
		for (uint8_t j=0; j<=MESSAGE_SIZE_1/2;j++) 
			encrypted_string[j] = cypher_fnc(encrypted_string[j], public->key_1, public->key_2);

		printf("PROC-A write data: ");
		for (uint8_t i=0; i<MESSAGE_SIZE_1/2; i++) {
			// load encrypted message into shared memory[0..9]
			memory_ptr[i] = encrypted_string[i];
			sleep(WRITE_DELAY_S);
			printf("%d ", memory_ptr[i]);
		}
		printf("\n");

		// allow subprocess to get message from shared memory
		kill(partner_pid, SIGUSR1);
		// wait for subprocess to load a new message or user to terminate programm
		if (wait_for_signal() == TERMINATE_SIGNAL) break;

		// load new message from shared memory [10..19]
		for(uint8_t k=MESSAGE_SIZE_1/2; k<MESSAGE_SIZE_1; k++)
			encrypted_string[k-MESSAGE_SIZE_1/2] = decypher_fnc(memory_ptr[k], private.key_1, private.key_2);

		// decrypt message set by subprocess and print to user
		char* decoded_string = decode_string(encrypted_string, MESSAGE_SIZE_1/2);
		printf("\nDecoded Process-A string: %s\n", decoded_string);
		free(decoded_string);

		// move the last letter set by subprocess one letter foward
		encrypted_string[MESSAGE_SIZE_1/2-1] = iterate_encoded_letter(encrypted_string[MESSAGE_SIZE_1/2-1]);
	}
	kill(partner_pid, SIGTERM);	

	printf("\n");
	// free allocations and shared memory
	free(public);
	free(encrypted_string);
	shmdt((const void*) memory_ptr);	

	return(OK);
}

err_t sub_process(){
	pid_t my_pid = getpid();	
	printf("Subprocess B, with pid %d\n", my_pid);
	
	printf("Searching for Process A pid in shared memory\n");
	pid_t partner_pid;
	while(true){
		partner_pid = get_partner_pid('a');
		if (partner_pid == 0){
			printf("Failed.. retrying in 1 second\n");
			sleep(1);
		}else{
			printf("Success. Process A pid: %d\n", partner_pid);
			break;
		}
	}

	printf("Saving current pid into shared memory\n");
	if (set_mysm_pid('B', my_pid) != OK) return ERR_PID;
	
	// wait for main process to finish seting up
	printf("Wainting for Process A\n");
	kill(partner_pid, SIGUSR1);
	wait_for_signal();

	// get public key from shared memory
	printf("Geting public encryption keys\n");
	encryp_key_t* public = get_public_keys();            // get public encryption keys from shared memory
	if (public == NULL) return(ERR_ENCRYPT_KEY);
	encryp_key_t private = { .key_1 = 7, .key_2 = 19};   // load private encryption key from local function

	key_t memory_key;	
	int  memory_id;
	uint8_t* memory_ptr;

	// get shared memory
	printf("Getting shared memory\n");
	memory_key = ftok(FILE_NAME_1, NUM_1);
	if(memory_key == -1) return(ERR_KEY);
	memory_id = shmget(memory_key, MESSAGE_SIZE_1*sizeof(uint8_t), 0666|IPC_CREAT);
	if(memory_id == -1) return(ERR_IDENTIFIER);
	memory_ptr = (uint8_t*)shmat(memory_id, (const void *)0, 0);
	if(memory_ptr == NULL) return(ERR_MEM_POINTER);

	uint8_t* encrypted_string = (uint8_t*)malloc(sizeof(uint8_t*)*MESSAGE_SIZE_1);

	wait_for_signal();
	
	while(1){
		// get shared memory and decrypt with local private key
		for(uint8_t i=0; i<MESSAGE_SIZE_1/2; i++)
			encrypted_string[i]  = decypher_fnc(memory_ptr[i], private.key_1, private.key_2);

		// decrypt message set by main process and print to user
		char* decoded_string = decode_string(encrypted_string, MESSAGE_SIZE_1/2);
		printf("\nDecoded Process-B string: %s\n", decoded_string);
		free(decoded_string);

		// move the last letter set by main process one letter foward
		encrypted_string[MESSAGE_SIZE_1/2-1] = iterate_encoded_letter(encrypted_string[MESSAGE_SIZE_1/2-1]);

		for (uint8_t j=0; j<=MESSAGE_SIZE_1/2;j++) {
			// encrypt message 
			encrypted_string[j] = cypher_fnc(encrypted_string[j], public->key_1, public->key_2);
		}

		printf("PROC-B write data: "); 
		for (uint8_t i=MESSAGE_SIZE_1/2; i<MESSAGE_SIZE_1; i++) {
			// load encrypted message into shared memory
			memory_ptr[i] = encrypted_string[i-MESSAGE_SIZE_1/2];
			sleep(WRITE_DELAY_S);
			printf("%d ", memory_ptr[i]);
		}
		printf("\n");

		kill(partner_pid, SIGUSR1);
		// wait for main process to load a new message or user to terminate programm
		if (wait_for_signal() == TERMINATE_SIGNAL) break;
	}

	printf("\n");
	// free allocations and shared memory
	free(public);
	free(encrypted_string);
	shmdt((const void*) memory_ptr);	
	return(OK);
}

err_t set_mysm_pid(char my_id, pid_t my_pid){
	
	if (my_id != 'A' && my_id != 'B') return ERR_PID;

	printf("Saving pid into memory of process %c\n", my_id);

	key_t memory_key;	
	int  memory_id;
	pid_t* memory_ptr;

	memory_key = ftok(FILE_NAME_3, NUM_3);
	if(memory_key == -1) return(ERR_KEY);
	memory_id = shmget(memory_key, MESSAGE_SIZE_3*sizeof(pid_t), 0666|IPC_CREAT);
	if(memory_id == -1) return(ERR_IDENTIFIER);
	memory_ptr = (pid_t*)shmat(memory_id, (const void *)0, 0);
	if(memory_ptr == NULL) return(ERR_MEM_POINTER);
	
	if(my_id == 'A'){
		memory_ptr[0] = my_pid;
		memory_ptr[1] = my_pid+10;
	}else{
		memory_ptr[2] = my_pid;
		memory_ptr[3] = my_pid+10;
	}

	shmdt((const void*) memory_ptr);	
	
	return(OK);
}

pid_t get_partner_pid(char partner){
	
	if (partner != 'A' && partner != 'B') return 0;

	printf("Geting pid of process %c\n", partner);

	key_t memory_key;	
	int  memory_id;
	pid_t* memory_ptr;
	pid_t target_pid;

	memory_key = ftok(FILE_NAME_3, NUM_3);
	if(memory_key == -1) return(ERR_KEY);
	memory_id = shmget(memory_key, MESSAGE_SIZE_3*sizeof(pid_t), 0666|IPC_CREAT);
	if(memory_id == -1) return(ERR_IDENTIFIER);
	memory_ptr = (pid_t*)shmat(memory_id, (const void *)0, 0);
	if(memory_ptr == NULL) return(ERR_MEM_POINTER);
	
#ifdef DEBUG_EN
	printf("memory: %d %d %d %d\n", memory_ptr[0], memory_ptr[1], memory_ptr[2], memory_ptr[3]);
#endif
	if (memory_ptr[0]+10 != memory_ptr[1] && memory_ptr[2]+10 != memory_ptr[3]) return 0;

	if(partner == 'A'){
		target_pid = memory_ptr[0];
	}else{
		target_pid = memory_ptr[2];
	}

	shmdt((const void*) memory_ptr);	
	
	return(target_pid);
}

encryp_key_t* get_public_keys(){
	
	printf("Geting public keys from shared memory\n");

	key_t memory_key;	
	int  memory_id;
	uint8_t* memory_ptr;

	memory_key = ftok(FILE_NAME_2, NUM_2);
	if(memory_key == -1) return(NULL);

	memory_id = shmget(memory_key, MESSAGE_SIZE_2*sizeof(uint8_t), 0666|IPC_CREAT);
	if(memory_id == -1) return(NULL);

	memory_ptr = (uint8_t*)shmat(memory_id, (const void *)0, 0);
	if(memory_ptr == NULL) return(NULL);
	
	// if shared memory is OK then allocate pointer key-structure, load local values and return pointer
	encryp_key_t* public = malloc(sizeof(encryp_key_t*));

	public -> key_1 = *(memory_ptr);
	public -> key_2 = *(memory_ptr+1);

	shmdt((const void*) memory_ptr);	
	
	return(public);
}

err_t load_public_keys(){
	
	printf("Loading public keys into shared memory\n");

	encryp_key_t public = { .key_1 = 4, .key_2 = 5 };

	key_t memory_key;	
	int  memory_id;
	uint8_t* memory_ptr;

	memory_key = ftok(FILE_NAME_2, NUM_2);
	if(memory_key == -1) return(ERR_KEY);

	memory_id = shmget(memory_key, MESSAGE_SIZE_2*sizeof(uint8_t), 0666|IPC_CREAT);
	if(memory_id == -1) return(ERR_IDENTIFIER);

	memory_ptr = (uint8_t*)shmat(memory_id, (const void *)0, 0);
	if(memory_ptr == NULL) return(ERR_MEM_POINTER);
	
	memory_ptr[0] = public.key_1;
	memory_ptr[1] = public.key_2;

	shmdt((const void*) memory_ptr);	

	return(OK);
}

#ifdef DEBUG_EN
void print_ret_info(err_t ret){
	switch(ret){
		case(ERR_KEY):
			printf("Could not get shared-memory key\n");
			break;
		case(ERR_IDENTIFIER):
			printf("Could not get shared-memory identifier\n");
			break;
		case(ERR_MEM_POINTER):
			printf("Could not get shared-memory pointer\n");
			break;
		case(ERR_ENCRYPT_KEY):
			printf("Could not get encryption keys\n");
			break;
		case(ERR_PARAM):
			printf("Error in program input parameter\n");
			break;
		case(ERR_PID):
			printf("Error in PID save/get to/from shared memeory\n");
			break;
		case(OK):
			printf("All OK\n");
			break;
		default:
			printf("UNKNOWN err code: %d\n", ret);
	}
}
#endif

int main(int argc, char **argv){

	char selected_process;
	if (argc == 2){
		if(!strcmp(argv[1],"proca")){
			printf("Running as Process A\n");
			selected_process = 'A';
		}else if(!strcmp(argv[1],"procb")){
			printf("Running as Process B\n");
			selected_process = 'B';
		}else{
			printf("Invalid paramether. Terminating.\n");
			return ERR_PARAM;
		}
	}else{
		printf("Enter ONE parameter for process selection. Enter 'proca' or 'procb'.\n");
		return ERR_PARAM;
	}

	err_t ret;

	if(selected_process == 'A'){
		if ((ret = load_public_keys()) != OK){
#ifdef DEBUG_EN
			printf("ERR loading public keys. ret: ");
			print_ret_info(ret);
#endif
		}else{
			ret = main_process();
#ifdef DEBUG_EN
			printf("Main process ret: ");
			print_ret_info(ret);
#endif
		}
	}else{
		ret = sub_process();
#ifdef DEBUG_EN
		printf("Sub-process ret: ");
		print_ret_info(ret);
#endif
	}

	return(ret);
}
