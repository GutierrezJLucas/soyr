# TRABAJO PRACTICO 1

## INSTRUCCIONES
Para ejecutar el script shell abrir una instancia de terminal en el directorio TP1 y tipear:
```
./tp1.sh -f DIR -r
```
La opción **-f** requiere de un argumento que coincida con el directorio que quiere analizarse. 

La opcion **-r** es opcional e indica que el análisis debe hacerse recursivamente por sobre el directorio especificado. Es decir se deben analizar todos los directorios contenidos. 
