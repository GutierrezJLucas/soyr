while getopts f:r flag # -f option with argument, -r option withow argument
do
	case "${flag}" in
		f) file_path=${OPTARG};; # save argument of -f option in $file_path
		r) recursive=1;; # if -r present set recursive flag
	esac
done


if [ -z "$file_path" ]; then # if file_path is not present then option argument has not been passed. print error and exit with code 2
	echo "Please select directory with -f option."
	exit 2
else
	if [ ! -d "$file_path" ]; then # if file_path is not a directory OR directory dont exist, then print error and exit with code 0
		echo "File path do not exist."
		exit 0
	else
		if [ "$recursive" = 1 ]; then
			str="" # if recursive skip maxdepth option
		else
			str="-maxdepth 1" # 1 is only the selected dir
		fi
		# load each amount in diff variable for arith operation and clear echo statement
		NUM_FILESnH=$(find $file_path $str -type f -not -path '*/.*' | wc -l); # skip any regular file starting with a dot
		NUM_FILES=$(find $file_path $str -type f | wc -l); 
		NUM_SYMBOLIC=$(find -L $file_path $str | wc -l); 
		NUM_DIRSnH=$(find $file_path $str -mindepth 1 -type d -not -path '*/.*' | wc -l); # mindepth 1 so it doesn't count the . for the current dir
		NUM_DIRS=$(find $file_path $str -mindepth 1 -type d | wc -l); # mindepth 1 so it doesn't count the . for the current dir
		NUM_ALL=$(find $file_path $str -mindepth 1 | wc -l);
		NUM_REST=$((NUM_ALL - NUM_FILES - NUM_DIRS));
		NUM_HIDDEN_FILES=$((NUM_FILES-NUM_FILESnH));
		NUM_HIDDEN_DIRS=$((NUM_DIRS-NUM_DIRSnH));
		echo "Directories in $file_path: Directories: $NUM_DIRSnH (+Hidden: $NUM_HIDDEN_DIRS) | Regular files: $NUM_FILESnH (+Hidden: $NUM_HIDDEN_FILES) | Rest: $NUM_REST"; 
	fi
fi
